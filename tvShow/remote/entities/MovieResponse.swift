//
//  MovieResponse.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/24/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
import ObjectMapper

class MovieResponse: Mappable {
    var title: String
    var year: Int
    var tagline: String
    var released: String
    var overview: String
    var trailer: String
    var homePage: String
    var rating: Float
    var images: [String]
    var genres: [String]
    var ids: IdsMovieResponse?
    
    required public init?(map: Map) {
        self.title = ""
        self.year = 0
        self.tagline = ""
        self.released = ""
        self.overview = ""
        self.trailer = ""
        self.homePage = ""
        self.rating = 0.0
        self.images = []
        self.genres = []
    }
    
    func mapping(map: Map){
        self.title <- map["title"]
        self.year <- map["year"]
        self.tagline <- map["tagline"]
        self.released <- map["released"]
        self.overview <- map["overview"]
        self.trailer <- map["trailer"]
        self.homePage <- map["homepage"]
        self.rating <- map["rating"]
        self.images <- map["images"]
        self.genres <- map["genres"]
        self.ids <- map["ids"]
    }
    
    static func toMoviesList(moviesResponse: [MovieResponse]) -> [Movie]{
        var movieList: [Movie] = []
        for movie in moviesResponse{
            movieList.append(movie.toMovie())
        }
        return movieList
    }
    
    static func toSearchMovieList(movieSearchResponse: [MovieSearchResponse])-> [Movie]{
        var movieList: [Movie] = []
        for movie in movieSearchResponse{
            movieList.append((movie.movie?.toMovie())!)
        }
        return movieList
    }
    
    func toMovie() -> Movie {
        return Movie(title: self.title, year: self.year, tagline: self.tagline, released: self.released, trailer: self.trailer, homePage: self.homePage, rating: self.rating, images: self.images, genres: self.genres,overview: self.overview, ids: self.ids!.toIdsMovie())
    }
}

class IdsMovieResponse : Mappable{
    var trakt: Int
    var slug: String
    var imdb: String
    var tmdb: Int
    
    required public init?(map: Map) {
        self.trakt = 0
        self.slug = ""
        self.imdb = ""
        self.tmdb = 0
    }
    
    func mapping(map: Map){
        self.trakt <- map["trakt"]
        self.slug <- map["slug"]
        self.imdb <- map["imdb"]
        self.tmdb <- map["tmdb"]
    }
    
    func toIdsMovie() -> IdsMovie {
        return IdsMovie(trakt: self.trakt, slug: self.slug, imdb: self.imdb, tmdb: self.tmdb)
    }
}
