//
//  ImagesResponse.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/27/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
import ObjectMapper

class ImageResponse: Mappable {
    var backdrops: [BackdropsResponse]
   
    required public init?(map: Map) {
        self.backdrops = []
    }
    
    func mapping(map: Map){
        self.backdrops <- map["backdrops"]
    }
    
    func getImage() -> String {
        var image = ""
        if self.backdrops.count > 0{
            image = "https://image.tmdb.org/t/p/w500\(self.backdrops[0].filePath)"
        }
        return image
    }
}

class BackdropsResponse: Mappable {
    var filePath: String
    
    required init?(map: Map) {
        self.filePath = ""
    }
    
    func mapping(map: Map) {
        self.filePath <- map["file_path"]
    }
}
