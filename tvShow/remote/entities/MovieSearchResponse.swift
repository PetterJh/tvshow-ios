//
//  MovieSearchResponse.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/25/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
import ObjectMapper

class MovieSearchResponse: Mappable{
    var type: String
    var score: Float 
    public var movie: MovieResponse?
    
    required public init?(map: Map) {
        self.type = ""
        self.score = 0
    }
    
    func mapping(map: Map){
        self.type <- map["type"]
        self.score <- map["score"]
        self.movie <- map["movie"]
    }
}
