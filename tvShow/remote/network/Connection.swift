//
//  Connection.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
import Alamofire

public class Connection : SessionManager{
    
    public static let sharedInstance = Connection()
    
    let configuration = URLSessionConfiguration.default
    
    public let headers: HTTPHeaders = [
        "Content-Type": "application/json",
        "trakt-api-version": "2",
        "trakt-api-key": "704d91a571d865b070f25086f44c21d22527072e9160808bca73c8137a01e80f"
    ]
    
    private init() {      
        self.configuration.timeoutIntervalForRequest = 10
        self.configuration.httpAdditionalHeaders = headers
        super.init(configuration: configuration, delegate: SessionManager.default.delegate, serverTrustPolicyManager:nil)
    }
}
