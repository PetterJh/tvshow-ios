//
//  Route.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation

enum Route {
    case movies
    case searchMovies
    case images
    
    func getUrl() -> String {
        let urlBase = ProductionConfig.serverURL
        let imageUrl = ProductionConfig.imagesUrl
        switch self {
        case .movies:
            return urlBase + "/movies/popular"
        case .searchMovies:
            return urlBase + "/search/movie"
        case .images:
            return imageUrl + "movie/"
        }
    }
}
