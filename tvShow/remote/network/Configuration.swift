//
//  Configuration.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
public protocol Configuration {
    static var serverURL: String { get }
}

public struct ProductionConfig: Configuration {
    public static let serverURL = "https://api.trakt.tv"
    public static let imagesUrl = "https://api.themoviedb.org/3/"
}

