//
//  Utils.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/24/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
import Alamofire

class Utils {
    public static func getDictionary(_ dataResponse:DataResponse<Any>) -> Dictionary<String, AnyObject>{
        return (dataResponse.value as? Dictionary<String, AnyObject>)!
    }
    
    public static func getJson(_ dataResponse: DataResponse<Any>) throws -> [[String : Any]]{
        print(dataResponse.data!)
      let json : Any! = try JSONSerialization.jsonObject(with: dataResponse.data!, options: [])
        print(json)
        return json as! [[String : Any]]
    }
}
