//
//  Extension.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/25/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation

extension String {
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
