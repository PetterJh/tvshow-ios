//
//  CloudMovieRepository.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
import Alamofire
import Alamofire_Synchronous
import AlamofireObjectMapper
import ObjectMapper

class CloudMovieRepository: MovieRepositoryRemote {
    
    func getMovies(page: Int) throws -> [Movie] {
        let parameters : [ String : Any] = [
            "page" : page,
            "limit" : 10,
            "extended": "full"
        ]
        
        var movies:[Movie] = []
        let dataResponse = Connection.sharedInstance.request(Route.movies.getUrl(), parameters: parameters).responseJSON()
        if let error = dataResponse.error {
            throw error
        } else {
            let moviesResponse = try Mapper<MovieResponse>().mapArray(JSONArray: Utils.getJson(dataResponse))
           
            movies = MovieResponse.toMoviesList(moviesResponse: moviesResponse)
            return movies
        }
    }
    
    func searchMovies(query: String, page: Int) throws -> [Movie] {
        let parameters : [ String : Any] = [
            "query" : query.trim(),
            "page" : page,
            "limit" : 10,
            "extended": "full"
        ]
        
        var movies:[Movie] = []
        
        let dataResponse = Connection.sharedInstance.request(Route.searchMovies.getUrl(), parameters: parameters).responseJSON()
        if let error = dataResponse.error {
            throw error
        } else {
            let moviesResponse = Mapper<MovieSearchResponse>().mapArray(JSONArray: try Utils.getJson(dataResponse))
            
            movies = MovieResponse.toSearchMovieList(movieSearchResponse: moviesResponse)
            return movies
        }
    }
    
    func getImage(tmdb: Int) throws -> String {
        let parameters : [ String : Any] = [
            "api_key" : "014c0be7d3f32777b58d2c2cbd921128"
        ]
        var image: String = ""
        let url = "\(Route.images.getUrl())\(tmdb)/images"
        let dataResponse = Connection.sharedInstance.request(url,parameters: parameters, headers: nil).responseJSON()
        
        if let error = dataResponse.error {
            throw error
        } else {
            let imageResponse = Mapper<ImageResponse>().map(JSON: Utils.getDictionary(dataResponse))
            image = (imageResponse?.getImage())!
            return image
        }
    }
}
