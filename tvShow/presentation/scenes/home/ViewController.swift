//
//  ViewController.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import UIKit

class ViewController: UIViewController, HomeView, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate{

    @IBOutlet weak var searchMovie: UISearchBar!
    @IBOutlet weak var tableMovies: UITableView!
    
    var page: Int = 1
    var searchPage: Int = 0
    
    var presenter: HomePresenter!
    
    var movieList: [Movie] = []
    var currentList: [Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpView() {
        let movieRepositoryRemote: MovieRepositoryRemote = CloudMovieRepository()
        let movieUseCase = MovieUseCase(movieRepositoryRemote: movieRepositoryRemote)
        self.presenter = HomePresenter(movieUseCase: movieUseCase, homeView: self)
        self.presenter.loadMovies(page: self.page)
    }
    
    func showLoading() {
        
    }
    
    func hiddenLoading() {
        
    }
    
    func showError(error: String) {
        
    }
    
    func showMovies(movies: [Movie]) {
        self.movieList.append(contentsOf: movies)
        self.currentList = self.movieList
        self.tableMovies.reloadData()
        self.page = self.page + 1
    }
    
    func showMoviesSearch(movies: [Movie]) {
        self.currentList = movies
        self.tableMovies.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let row = indexPath.row
        if row == self.currentList.count - 1{
            self.presenter.loadMovies(page: page)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.currentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = self.currentList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "moviesCell") as! MovieViewCell
        cell.loadData(movie)
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else{
            self.currentList = self.movieList
            self.searchPage = 1
            self.tableMovies.reloadData()
            return
        }
        self.searchPage = 1
        self.presenter.searchMovies(query: searchText,page: searchPage)
    }
    
}

