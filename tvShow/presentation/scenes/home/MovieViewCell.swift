//
//  MovieViewCell.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/25/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import UIKit

class MovieViewCell: UITableViewCell {
    
    @IBOutlet weak var imageMovie: UIImageView!
    @IBOutlet weak var nameMovie: UILabel!
    @IBOutlet weak var releaseMovie: UILabel!
    @IBOutlet weak var overviewMovie: UILabel!
    
    func loadData(_ movie: Movie) {
        self.nameMovie.text = movie.title
        self.releaseMovie.text = String(movie.year)
        self.overviewMovie.text = movie.overview
        if !movie.pathImage.isEmpty {
            let url = NSURL(string: movie.pathImage)
            if let data = NSData(contentsOf: url! as URL){
                self.imageMovie.contentMode = UIViewContentMode.scaleAspectFit
                self.imageMovie.image = UIImage(data: data as Data)
            }
        }
    }
}
