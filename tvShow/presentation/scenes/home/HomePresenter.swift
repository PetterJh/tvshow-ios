//
//  HomePresenter.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
import RxSwift

class HomePresenter : Presenter {
   
    var homeView: HomeView
    var movieUseCase: MovieUseCase
    var composite: CompositeDisposable!
    
    init(movieUseCase: MovieUseCase, homeView: HomeView) {
        self.movieUseCase = movieUseCase
        self.homeView = homeView
    }
    
    func loadMovies(page: Int){
      _ = Single<[Movie]>.create{
            single in
            do
            {
                let response:[Movie] = try self.movieUseCase.getMovies(page: page)
                single(.success(response))
            } catch let error as NSError {
                single(.error(error))
            }
            return Disposables.create()
        }
        .observeOn(MainScheduler.instance)
        .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
        .subscribe(
            onSuccess: {
                self.homeView.hiddenLoading()
                self.homeView.showMovies(movies: $0)},
            onError: {
                self.homeView.hiddenLoading()
                self.homeView.showError(error: $0.localizedDescription)})
    }
    
    func searchMovies(query: String, page: Int) {
        _ = Single<[Movie]>.create{
            single in
            do
            {
                let response:[Movie] = try self.movieUseCase.searchMovies(query: query,page: page)
                single(.success(response))
            } catch let error as NSError {
                single(.error(error))
            }
            return Disposables.create()
            }
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onSuccess: {
                    self.homeView.hiddenLoading()
                    self.homeView.showMoviesSearch(movies: $0)},
                onError: {
                    self.homeView.hiddenLoading()
                    self.homeView.showError(error: $0.localizedDescription)})
    }
    
    
    func initPresenter() {
        self.composite = CompositeDisposable()
    }
    
    func didDisapear() {
        self.composite.dispose()
    }
}

protocol HomeView : LoadView{
    func showMovies(movies: [Movie])
    func showMoviesSearch(movies: [Movie])
}
