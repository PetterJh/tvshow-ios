//
//  LoadView.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
protocol LoadView {
    func showLoading()
    func hiddenLoading()
    func showError(error: String)
}
