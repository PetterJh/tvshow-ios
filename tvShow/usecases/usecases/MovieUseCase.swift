//
//  MovieUseCase.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
class MovieUseCase {
    
    var movieRepositoryRemote  : MovieRepositoryRemote
    init(movieRepositoryRemote: MovieRepositoryRemote) {
        self.movieRepositoryRemote = movieRepositoryRemote
    }
    
    func getMovies(page: Int) throws -> [Movie] {
        let movieList = try self.movieRepositoryRemote.getMovies(page:page)
        for movie in movieList {
            movie.pathImage = try self.movieRepositoryRemote.getImage(tmdb: movie.ids.tmdb)
        }
        return movieList
    }
    
    func searchMovies(query: String, page: Int)throws -> [Movie]{
        let movieList = try self.movieRepositoryRemote.searchMovies(query:query,page: page)
        for movie in movieList {
            movie.pathImage = try self.movieRepositoryRemote.getImage(tmdb: movie.ids.tmdb)
        }
        return movieList
    }
}
