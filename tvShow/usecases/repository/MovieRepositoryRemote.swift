//
//  MovieRepositoryRemote.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
protocol MovieRepositoryRemote {
    func getMovies(page:Int) throws -> [Movie]
    func searchMovies(query: String, page: Int)throws -> [Movie]
    func getImage(tmdb: Int) throws -> String
}
