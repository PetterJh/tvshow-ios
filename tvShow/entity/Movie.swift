//
//  Movie.swift
//  tvShow
//
//  Created by Petter Jhunior on 12/23/18.
//  Copyright © 2018 Petter Jhunior. All rights reserved.
//

import Foundation
class Movie {
    var title: String
    var year: Int
    var tagline: String
    var released: String
    var trailer: String
    var homePage: String
    var rating: Float
    var images: [String]
    var genres: [String]
    var overview: String
    var ids: IdsMovie
    var pathImage: String
    
    init(title: String, year: Int, tagline: String, released: String,
         trailer: String, homePage: String, rating: Float, images:[String],
         genres: [String], overview: String, ids: IdsMovie) {
        self.title = title
        self.year = year
        self.tagline = tagline
        self.released = released
        self.trailer = trailer
        self.homePage = homePage
        self.rating = rating
        self.images = images
        self.genres = genres
        self.overview = overview
        self.ids = ids
        self.pathImage = ""
    }
}

class IdsMovie {
    var trakt: Int
    var slug: String
    var imdb: String
    var tmdb: Int
    
    init(trakt: Int, slug: String, imdb: String, tmdb: Int) {
        self.trakt = trakt
        self.slug = slug
        self.imdb = imdb
        self.tmdb = tmdb
    }
}
